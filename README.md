# Welcome to Eaglercraft Extras!
This is a website similar to EaglerCrack (if you know it) where you can find all things related to Eaglercraft!
It is a solo developed project for now, and I will try to update it every other Friday.

## Features
- Find pre-built worlds to download and play!
- Search for servers to play with other people!
- Download resource packs to customize your Eaglercraft!
- Look at a <b>giant</b> selection of clients to play on!
- Download skins for Eaglercraft!

## Meet the Developer

### Hey there! I'm cobblesteve.
I started this project in April, and I wanted it to become the alternative for EaglerCrack.
I'm making it for a website I have right now, which currently uses EaglerCrack as a source.

Here are some things about me:
- Knows HTML and CSS
- Learning JavaScript and Python
- Barely knows Python
- Has a YouTube channel >>> https://youtube.com/@cobblesteve.official
- Likes to play Minecraft, Geometry Dash, A Dance of Fire and Ice (ADOFAI), and more!
- yea :D

